
<?php
    include "class.php";
    include "db.php";
    include "query.php";
?>
<html>
    <head>
        <title>Object Oriented PHP</title>
    </head>
    <body>
        <p>
        <?php
            $text = 'Hello World';
            echo "$text And The Universe";
            echo '<br>';
            $msg = new Message();
            echo '<br>';
            echo Message::$count;
            $msg->show();
            $msg1 = new Message("A new text");
            $msg1->show();
            echo '<br>';
            echo Message::$count;
            $msg2 = new Message();
            $msg2->show();
            echo '<br>';
            echo Message::$count;
            echo '<br>';
            echo '<br>';
            $msg3 = new redMessage("A red Message");
            $msg3->show();
            echo '<br>';
            $msg4 = new coloredMessage('A colored Message');
            $msg4 ->color = 'green';
             $msg4 ->show();
            echo '<br>';
            $msg4 ->color = 'yellow';
            $msg4 ->show();
            echo '<br>';
            $msg4 ->color = 'green';
            $msg4 ->show();
            echo '<br>';
            $msg4 ->color = 'black'; 
            $msg4 ->show();
            echo '<br>';
            $msg5 = new coloredMessage('A colored Message');
            $msg5 ->color = 'bleck';
            $msg5 ->show();
            $msg5 ->color = 'green';
            $msg5 ->color = 'bleck';
            echo '<br>';
            $msg5 ->show();
            echo '<br>';
            showObject($msg5);
            echo '<br>';   
            showObject($msg1);
            //database connection
            $db = new DB('localhost', 'intro', 'root', '');
            $dbc= $db->connect();
            #יצירת אובייקט להרצת השאילתה
            $query = new Query($dbc);
            #שאילתה ששולפת את הכל המידע מהטבלה שיצרנו במסד 
            $q = "SELECT * FROM users";
            #הרצת השאילתה
            $result = $query ->query($q);
           // echo $result->num_rows; 
           if($result->num_rows > 0){
            echo '<table>'; #הצגה טבלאית 
            #יצירת הכותרות לפי המידע שאנחנו רוצים להציג
            echo '<tr><th>Name</th><th>Email</th></tr>';
            while($row = $result->fetch_assoc()){
                echo '<tr>'; #פתיחת שורה
                #יצירת שורה בכדי לגשת לתוכן של הכותרות שבחרנו לעיל
                echo '<td>'.$row['name'].'</td><td>'.$row['email'].'</td>';
                echo '</tr>';#סגירת שורה
            }
            echo '</table>';

             } else{ #למקרה שאין רשומות/אין תוצאות למה שרציתי
                 echo "Sorry no results";
            }
           



            
        ?>
        </P>
    </body>
</html>
